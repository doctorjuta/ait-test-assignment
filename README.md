# AIT test assignment

Test assignment project for AIT company

How to use:

```
from ds import DataScrapper
ds = DataScrapper()
products = ds.scrape_products()
for t in json.loads(products):
    print(t['name'])
```

### Main class

**ds = DataScrapper(usr='admin', pwd='12345')**

### Methods

**ds.login(use_proxy=False)** - login method. You can set own username and password as params during main object creating. Return JSON string with session value and logged_in indicator.

**ds.scrape_products()** - method for getting all products data from all pages. Return JSON string with list of products. Each element contain 'name', 'description', 'is_best_price' indicator, 'price' value and 'discount' value.

**ds.scrape_cities()** - method for getting all cities data from all pages. Return properly formatted string for using in CSV.

# Queries for MySQL tests

`SELECT * FROM Employees WHERE BirthDate >= '1950-01-01' AND BirthDate <= '1960-12-31';`

`SELECT * FROM Products WHERE CategoryID = 1;`

`SELECT Products.* FROM Products JOIN OrderDetails ON Products.ProductID = OrderDetails.ProductID JOIN Orders ON Orders.OrderID = OrderDetails.OrderID WHERE OrderDetails.Quantity > 10 AND YEAR(Orders.OrderDate) = 1997;`
