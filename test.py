"""Main tests for DataScrapper."""
import unittest
import json
from ds import DataScrapper


class TestDataScrapper(unittest.TestCase):
    """Main class for testing DataScrapper application."""

    def setUp(self):
        """Init method for all tests."""
        self.ds = DataScrapper()

    def test_correct_login_request(self):
        """Test correct login."""
        raw_result = self.ds.login()
        result = json.loads(raw_result)
        self.assertEqual(result['logged_in'], True)

    def test_incorrect_login_request(self):
        """Test incorrect login."""
        invalid_ds = DataScrapper(usr='admin1', pwd='54321')
        raw_result = invalid_ds.login()
        result = json.loads(raw_result)
        self.assertEqual(result['logged_in'], False)

    @unittest.skip("We skip long-lasting tests in usual testing process.")
    def test_correct_login_request_with_proxy(self):
        """Test correct login with using proxy."""
        raw_result = self.ds.login(use_proxy=True)
        result = json.loads(raw_result)
        self.assertEqual(result['logged_in'], True)

    def test_products_scrape_get_all_products(self):
        """Test that products scrape method find all products on all pages."""
        raw_result = self.ds.scrape_products()
        result = json.loads(raw_result)
        self.assertEqual(len(result), 64)

    def test_products_scrape_all_names(self):
        """Test that products scrape method get all products names."""
        raw_result = self.ds.scrape_products()
        result = json.loads(raw_result)
        for it in result:
            self.assertNotEqual(it['name'], '')

    def test_products_scrape_all_descriptions(self):
        """Test that products scrape method get all products descriptions."""
        raw_result = self.ds.scrape_products()
        result = json.loads(raw_result)
        for it in result:
            self.assertNotEqual(it['description'], [])

    def test_products_scrape_all_prices(self):
        """Test that products scrape method get all products prices."""
        raw_result = self.ds.scrape_products()
        result = json.loads(raw_result)
        for it in result:
            self.assertNotEqual(it['price'], 0)

    def test_cities_data_scraper(self):
        """Test that cities scrape method find all cities on all pages."""
        raw_result = self.ds.scrape_cities()
        self.assertEqual(raw_result.count('\n'), 13)
