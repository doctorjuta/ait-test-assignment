"""Application for scrapping data from playground website."""
import requests
from bs4 import BeautifulSoup
import json
import re


class DataScrapper:
    """Main class for scapping data."""

    _BASE_URL = "http://testing-ground.scraping.pro"
    _LOGIN_URL = "http://testing-ground.scraping.pro/login"
    products = []
    cities = {}

    def __init__(self, usr='admin', pwd='12345'):
        """Init object."""
        self.usr = usr
        self.pwd = pwd
        pass

    def login(self, use_proxy=False):
        """Login on main page."""
        if use_proxy:
            proxies = self.get_proxy_list()
        else:
            proxies = [False]
        result = {
            'session': '',
            'logged_in': False
        }
        post_data = {
            'usr': self.usr,
            'pwd': self.pwd
        }
        for proxy in proxies:
            if proxy:
                proxies = {
                    'http': proxy,
                    'https': proxy
                }
            else:
                proxies = {}
            with requests.Session() as s:
                try:
                    r = s.post(
                        self._LOGIN_URL + '?mode=login',
                        data=post_data,
                        allow_redirects=False,
                        proxies=proxies
                    )
                except requests.Timeout:
                    continue
                if 'tdsess' in r.cookies:
                    result['session'] = r.cookies['tdsess']
                try:
                    r = s.post(
                        self._LOGIN_URL + '?mode=welcome',
                        cookies=r.cookies,
                        proxies=proxies
                    )
                except requests.Timeout:
                    continue
                if "<h3 class='success'>WELCOME :)</h3>" in r.text:
                    result['logged_in'] = True
                    break
        return json.dumps(result)

    def get_proxy_list(self):
        """Read CSV file with proxies and return ready to use dict."""
        import csv
        proxies_list = []
        with open('proxies.csv') as f:
            rd = csv.reader(f, delimiter=',')
            next(rd)
            for row in rd:
                proxies_list.append(row[0])
        return proxies_list

    def scrape_products(self):
        """Method for scrapping products data."""
        self.products = []
        links = self.get_pages_url('blocks')
        for link in links:
            r = requests.get(self._BASE_URL + link)
            soup = BeautifulSoup(r.text, "html.parser")
            case1 = soup.find(id='case1')
            if case1:
                self.scrape_case1_products(case1)
            case2 = soup.find(id='case2')
            if case2:
                self.scrape_case2_products(case2)
        return json.dumps(self.products)

    def scrape_case1_products(self, soup):
        """Scrape products for block with case1 id."""
        for d in soup.findAll('div', {'class': ['prod1', 'prod2']}):
            product_name = self.scrape_product_name(d)
            product_desc = self.scrape_product_description(d)
            product_discound = self.scrape_product_discound(d)
            product_bp = self.scrape_product_bestprice(d)
            product_price = self.scrape_product_price(d)
            self.products.append({
                'name': product_name,
                'description': product_desc,
                'is_best_price': product_bp,
                'price': product_price,
                'discound': product_discound
            })

    def scrape_case2_products(self, soup):
        """Scrape products for block with case2 id."""
        indx = 0
        p_n = soup.findAll('div', {'class': ['prod1', 'prod2']})
        p_p = soup.findAll('div', {'class': ['price1', 'price2']})
        for d in p_n:
            product_name = self.scrape_product_name(d)
            product_desc = self.scrape_product_description(d)
            product_discound = self.scrape_product_discound(p_p[indx])
            product_bp = self.scrape_product_bestprice(p_p[indx])
            product_price = self.scrape_product_price(p_p[indx])
            self.products.append({
                'name': product_name,
                'description': product_desc,
                'is_best_price': product_bp,
                'price': product_price,
                'discound': product_discound
            })
            indx += 1

    def scrape_product_name(self, soup):
        """Get product name from html div."""
        name = ''
        n = soup.find('div', {'class': ['name']})
        if n:
            name = n.get_text()
        return name

    def scrape_product_description(self, soup):
        """Get product description from html div."""
        description = []
        s = soup.find('span', {'style': 'float: left'})
        if s:
            s.find('div', {'class': ['name']}).decompose()
            description = s.get_text().split(', ')
        else:
            soup.find('div', {'class': ['name']}).decompose()
            description = soup.get_text().split(', ')
        return description

    def scrape_product_price(self, soup):
        """Get product price from html div."""
        price = 0
        div = soup.find('div')
        if div:
            div.decompose()
        s = soup.find('span', {'style': 'float: right'})
        if s:
            txt_price = s.get_text()
        else:
            txt_price = soup.get_text()
        if txt_price:
            price = float(re.sub(r'[^\d.]', '', txt_price))
        return price

    def scrape_product_bestprice(self, soup):
        """Check if product marked as 'best price'."""
        best_price = False
        bp_div = soup.findAll('span', {'class': 'best'})
        if bp_div:
            best_price = True
        if 'best' in soup['class']:
            best_price = True
        return best_price

    def scrape_product_discound(self, soup):
        """Check if product has discount."""
        discount = '0%'
        disc_div = soup.find('div', {'class': 'disc'})
        if disc_div:
            discount = disc_div.get_text().replace('discount ', '')
        return discount

    def get_pages_url(self, add_url):
        """Return links for all pages with additional URL."""
        links = []
        r = requests.get(self._BASE_URL + "/" + add_url)
        soup = BeautifulSoup(r.text, "html.parser")
        caseinfo = soup.find(id='caseinfo')
        if not caseinfo:
            return links
        for link in caseinfo.findAll('a'):
            links.append(link['href'])
        return links

    def scrape_cities(self):
        """Method for scrapping cities data."""
        links = self.get_pages_url('textlist')
        cities_data = self.reset_scrape_cities()
        for link in links:
            text_block = self.parse_cities_text_block(link)
            if not text_block:
                continue
            text_list = text_block.split('<br/>')
            for line in text_list:
                if len(line) < 5:
                    continue
                if '\n' in line:
                    line = line.replace('\n', '')
                if '<b>' in line:
                    line = line.replace('<b>', '')
                    line = line.replace('</b>', '')
                if '\xa0' in line:
                    self.add_cities_data(cities_data)
                    cities_data = self.reset_scrape_cities()
                    line_list = line.split('\xa0')
                    cities_data[0] = line_list[0]
                    cities_data[2] = '“' + line_list[-1] + '”'
                elif 'change: ' in line:
                    line_list = line.split(': ')
                    cities_data[3] = line_list[-1]
                elif ' in ' in line:
                    line_list = line.split(' in ')
                    cities_data[3] = self.calculate_diff(
                        line_list[0], cities_data[2]
                    )
                elif line[0] == '(':
                    cities_data[1] = line.replace(
                        '(', ''
                    ).replace(
                        ')', ''
                    )
                else:
                    self.add_cities_data(cities_data)
                    cities_data = self.reset_scrape_cities()
                    cities_data[0] = line.strip()
        self.add_cities_data(cities_data)
        return self.format_cities_data()

    def parse_cities_text_block(self, link):
        """Parse content from cities div and return formatted text."""
        r = requests.get(self._BASE_URL + link)
        soup = BeautifulSoup(r.text, "lxml")
        text_block = soup.find(id='case_textlist')
        if not text_block:
            return False
        text_block = str(text_block).replace(
            '<div id="case_textlist">', ''
        ).replace(
            '</div>', ''
        ).replace(
            '<b>CITY          POPULATION</b>', ''
        ).replace(
            '------------------------<br/>', ''
        )
        return text_block

    def reset_scrape_cities(self):
        """Return empty values for single string row with cities data.

        0 - city_name
        1 - city_alt_name
        2 - city_pop
        3 - city_pop_change
        """
        return ['null', 'null', 'null', 'null']

    def calculate_diff(self, f_number, s_number):
        """Calculate percent diff between two numbers."""
        f_number = f_number.replace('(', '').replace(')', '').replace(',', '')
        s_number = s_number.replace('“', '').replace('”', '').replace(',', '')
        res = round(100 - (int(f_number) * 100 / int(s_number)), 2)
        if res > 0:
            res = '+' + str(res) + '%'
        else:
            res = '-' + str(res) + '%'
        return res

    def add_cities_data(self, data):
        """Convert cities data to string and return."""
        if data.count('null') > 3:
            return False
        if data[0] == 'null':
            return False
        if data[0] not in self.cities:
            self.cities[data[0]] = {
                'alt_name': data[1],
                'pop': data[2],
                'pop_change': data[3]
            }
        if self.cities[data[0]]['alt_name'] == 'null':
            self.cities[data[0]]['alt_name'] = data[1]
        if self.cities[data[0]]['pop'] == 'null':
            self.cities[data[0]]['pop'] = data[2]
        if self.cities[data[0]]['pop_change'] == 'null':
            self.cities[data[0]]['pop_change'] = data[3]

    def format_cities_data(self):
        """Format cities prop and return string ready to using as CSV."""
        data = 'name, alternative name, population, changed\n'
        for city in self.cities:
            data += (
                f"{city}, "
                f"{self.cities[city]['alt_name']}, "
                f"{self.cities[city]['pop']}, "
                f"{self.cities[city]['pop_change']}\n"
            )
        return data
